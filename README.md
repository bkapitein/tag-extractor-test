# Undefined array key example

- run `composer install`
- run `php ./undefined-array-key.php`
- see that the PHP warning "Undefined array key "-0.00000" occurs
