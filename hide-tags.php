<?php
require __DIR__ . '/vendor/autoload.php';

$tags = [
    "{{plaatsing_startdatum}}",
    "{{plaatsing_einddatum}}",
    "{{relatie_naam}}",
    "{{vacature_beroep}}",
    "{{vacature_cao}}",
    "{{plaatsing_functieschaal}}",
    "{{plaatsing_werkuren}}",
    "{{bruteloon_perwerkuur}}",
    "{{project+plaatsing_gevraagdebedrijfsmiddelen}}",
];

$document = SetaPDF_Core_Document::loadByFilename("example.pdf");
$writer = new SetaPDF_Core_Writer_File("example-out.pdf");
$document->setWriter($writer);

$pages = $document->getCatalog()->getPages();

SetaPDF_Extractor_Strategy_Word::$characters .= '{}_';
$extractor = new SetaPDF_Extractor($document);
$extractor->setStrategy(new SetaPDF_Extractor_Strategy_Word());
try {
    for ($pageNo = 1; $pageNo <= $pages->count(); $pageNo++) {
        $words = $extractor->getResultByPageNumber($pageNo, \SetaPDF_Core_PageBoundaries::CROP_BOX);
        $page = $pages->getPage($pageNo);
        
        /** @var SetaPDF_Extractor_Result_Word $word */
        foreach ($words as $word) {
            if (in_array($word->getString(), $tags)) {
                echo "{$word}\n";
                $rect = $word->getBounds()[0]->getRectangle();
                $page
                    ->getContents()
                    ->encapsulateExistingContentInGraphicState();

                $page->getCanvas()
                    ->draw()
                    ->setNonStrokingColor(1)
                    ->rect(
                        $rect->getUl()->getX(),
                        $rect->getLl()->getY(),
                        $rect->getWidth(),
                        $rect->getHeight(),
                        \SetaPDF_Core_Canvas_Draw::STYLE_FILL
                    );
            }
        }
    }
}finally {
    $document->save()->finish();
}
