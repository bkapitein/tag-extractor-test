<?php
require __DIR__ . '/vendor/autoload.php';

$tags = [
    "{{koper1_handtekening}}",
    "{{koper1_ondertekentekst_datum}}",
    "{{verkoper1_ondertekentekst_datum}}",
    "{{koper1_ondertekentekst_plaats}}",
];

$document = SetaPDF_Core_Document::loadByFilename("undefined-array-key-example.pdf");

$pages = $document->getCatalog()->getPages();

SetaPDF_Extractor_Strategy_Word::$characters .= '{}_';
$extractor = new SetaPDF_Extractor($document);
$extractor->setStrategy(new SetaPDF_Extractor_Strategy_Word());
    for ($pageNo = 1; $pageNo <= $pages->count(); $pageNo++) {
        $words = $extractor->getResultByPageNumber($pageNo, \SetaPDF_Core_PageBoundaries::CROP_BOX);
        $page = $pages->getPage($pageNo);
    }
